import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split

from config import Config

np.random.seed(Config.RANDOM_SEED)

Config.ORIGINAL_DATASET_FILE_PATH.mkdir(parents=True, exist_ok=True)
Config.ORIGINAL_DATASET_FILE_PATH.parent.mkdir(parents=True, exist_ok=True)


df = pd.read_table('https://archive.ics.uci.edu/ml/machine-learning-databases/00291/airfoil_self_noise.dat',
                   names=['freq', 'atack_angle', 'chord_length', 'fs_velocity', 'displacement', 'sound_pressure'])

df.to_csv(str(Config.ORIGINAL_DATASET_FILE_PATH / "df.csv"), index=None)

