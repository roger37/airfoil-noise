import pickle
import parameters as pr
import pandas as pd
from sklearn.model_selection import cross_val_score, GridSearchCV
from sklearn.linear_model import LinearRegression, Ridge, Lasso, ElasticNet

from config import Config

Config.MODELS_PATH.mkdir(parents=False, exist_ok=True)

X = pd.read_csv(str(Config.FEATURES_PATH / "x.csv"))
y = pd.read_csv(str(Config.FEATURES_PATH / "y.csv"))

lr = LinearRegression()
ridge = GridSearchCV(Ridge(), pr.ridge_params, scoring='neg_root_mean_squared_error', cv=5).fit(X, y)
lasso = GridSearchCV(Lasso(max_iter=10000), pr.lasso_params, scoring='neg_root_mean_squared_error', cv=5).fit(X, y)
enet = GridSearchCV(ElasticNet(max_iter=10000), pr.enet_params, scoring='neg_root_mean_squared_error', cv=5).fit(X, y)

lr_score = -cross_val_score(lr, X, y, cv=5, scoring='neg_root_mean_squared_error').mean()
ridge_score = -ridge.best_score_
lasso_score = -lasso.best_score_
enet_score = -enet.best_score_

print('LINEAR REGRESSION  = Score: {}'.format(lr_score))
print('RIDGE  = Params:{}, Score: {}'.format(ridge.best_params_, ridge_score))
print('LASSO  = Params:{}, Score: {}'.format(lasso.best_params_, lasso_score))
print('ELASTIC NET  = Params:{}, Score: {}'.format(enet.best_params_, enet_score))

pickle.dump(lr, open(str(Config.MODELS_PATH / "model.pickle"), "wb"))
