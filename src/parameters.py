import yaml

with open("./params.yaml", 'r') as fd:
    params = yaml.safe_load(fd)

alphas = params['alphas']
l1_ratio = params['l1_ratio']

ridge_params = {
    'alpha': alphas
}

lasso_params = {
    'alpha': alphas
}

enet_params = {
    'alpha': alphas,
    'l1_ratio': l1_ratio
}
print(enet_params)