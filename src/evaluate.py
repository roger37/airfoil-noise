import json
import math
import pickle

import pandas as pd
from sklearn.model_selection import cross_val_score
from config import Config

X = pd.read_csv(str(Config.FEATURES_PATH / "x.csv"))
y = pd.read_csv(str(Config.FEATURES_PATH / "y.csv"))

model = pickle.load(open(str(Config.MODELS_PATH / "model.pickle"), "rb"))

lr_rmse = -cross_val_score(model, X, y, cv=5, scoring='neg_root_mean_squared_error').mean()
lr_mae = -cross_val_score(model, X, y, cv=5, scoring='neg_mean_absolute_error').mean()
lr_mape = -cross_val_score(model, X, y, cv=5, scoring='neg_mean_absolute_percentage_error').mean()

with open(str(Config.METRICS_FILE_PATH), "w") as outfile:
    json.dump(dict(rmse=lr_rmse, mae=lr_mae, mape=lr_mape), outfile)
