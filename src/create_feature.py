import pandas as pd
import seaborn as sns
from config import Config

Config.FEATURES_PATH.mkdir(parents=True, exist_ok=True)

df = pd.read_csv(str(Config.ORIGINAL_DATASET_FILE_PATH / "df.csv"))

X = df.drop(['sound_pressure'], axis=1)
y = df['sound_pressure']

X.to_csv(str(Config.FEATURES_PATH / "x.csv"), index=None)
y.to_csv(str(Config.FEATURES_PATH / "y.csv"), index=None)




