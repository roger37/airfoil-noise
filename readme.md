# AUTO-RUÍDO DE AEROFÓLIO

O conjunto de dados foi primeiramente descrito em um documento técnico da NASA publicado em [1]. É composto de medidas feitas em túnel de vento com diferentes tamanhos de aerofólio do tipo NACA 0012 e diferentes velocidades de vento. A medida realizada é a pressão sonora em forma logarítmica (dB). Essa medida de ruído é feita com a fonte e observado sempre no mesma posição.

# Atributos

indice | Atributos
----|------------------
1 | Frequency, in Hertzs
2 |Angle of attack, in degrees
3 |Chord length, in meters
4 |Free-stream velocity, in meters per second
5 |Suction side displacement thickness, in meters

## Referência

1. Thomas F Brooks, D Stuart Pope, and Michael A Marcolini. Airfoil self-noise and prediction. Vol. 1218.
National Aeronautics and Space Administration, Office of Management . . ., 1989